let burger = document.getElementById('burger')
let navigation = document.getElementById('nav')
let submit = document.querySelector('#submit')
let success = document.querySelector('#success')
let error = document.querySelector('#error')
let nameInput = document.querySelector('#name')
let emailInput = document.querySelector('#email')
let textarea = document.querySelector('#message')

burger.addEventListener('click', () => {
burger.classList.toggle('header__burger_active');
  navigation.classList.toggle('navigation__active');

})

submit.addEventListener('click', (event) => {
  event.preventDefault()

  if (nameInput.value && emailInput.value && textarea.value) {
    success.classList.add('wrapper-success_active')
    error.classList.remove('form__fields-error_active')
    nameInput.value = ''
    emailInput.value = ''
    textarea.value = ''

    setTimeout(() => {
      success.classList.remove('wrapper-success_active')
    }, 3000)
  } else {
    error.classList.add('form__fields-error_active')
  }
})